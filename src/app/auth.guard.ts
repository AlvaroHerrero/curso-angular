import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UsuarioService } from './usuario.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanLoad {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean|UrlTree>|Promise<boolean|UrlTree>|boolean|UrlTree {
    return this.validate();
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean|UrlTree>|Promise<boolean|UrlTree>|boolean|UrlTree {
    return this.validate();
  }

  private validate(): boolean {
    const estaLogueado = this.usuarioService.usuarioId !== null;
    if (!estaLogueado) {
      alert('no estas logueado');
    }
    return estaLogueado;
  }

  constructor(private usuarioService: UsuarioService) {
  }


}

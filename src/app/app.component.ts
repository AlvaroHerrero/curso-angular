import { Component, OnInit } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  // @ViewChild('myForm') myForm: NgForm;

  myFormGroup = this.formBuilder.group({
    nombre: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(10), Validators.email]],
    apellidos: ['gomez', [], [validateApellidosCustomAsync()]],
    aceptoCondiciones: ['', [Validators.requiredTrue]],
  }, {
    validators: [validateForm()],
  });


  activateRoute(component): void {
    // Log del componente que el router ha cargado
    // console.log(component);
  }

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private title: Title,
              private formBuilder: FormBuilder) {
  }

  submit(): void {

  }

  ngOnInit(): void {

    // this.myFormGroup.get('nombre').patchValue('sss');
    this.myFormGroup.get('apellidos').disable();


    this.myFormGroup.controls.nombre.valueChanges.subscribe(value => {
      console.log(value);
    });

    // this.router.events
    //   .pipe(
    //     filter(routeEvent => routeEvent instanceof NavigationEnd),
    //     map(() => this.activatedRoute.firstChild),
    //     switchMap(firstChild => firstChild.data),
    //   )
    //   .subscribe((data) => {
    //     this.title.setTitle(data.titulo);
    //   });
  }

}

function validateApellidosCustom(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors|null => {
    return control.value === 'gomez' ? null : {noEsGomez: true};
  };
}

function validateApellidosCustomAsync(): AsyncValidatorFn {
  return (control: AbstractControl): Observable<ValidationErrors|null> => {
    return of(control.value === 'gomez' ? null : {noEsGomez: true}).pipe(delay(4000));
  };
}

function validateForm(): ValidatorFn {
  return (form: FormGroup): Observable<ValidationErrors|null> => {
    console.log(form);
    return null;
  };
}




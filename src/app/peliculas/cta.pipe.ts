import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cta',
})
export class CtaPipe implements PipeTransform {
  transform(value: string|number): string {
    return `Consigue esta pelicula por ${value}!!`;
  }

}

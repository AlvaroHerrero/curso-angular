export interface Pelicula {
  id: number;
  titulo: string;
  descripcion?: string;
  genero?: 'miedo'|'aventura';
  precio: number;
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayLimit',
})
export class ArrayLimitPipe implements PipeTransform {

  transform(value: unknown[], limit = 3): unknown[] {
    return value.slice(0, limit);
  }

}

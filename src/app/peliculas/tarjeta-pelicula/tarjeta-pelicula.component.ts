import { Component, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';
import { Pelicula } from '../models';

@Component({
  selector: 'app-tarjeta-pelicula',
  templateUrl: './tarjeta-pelicula.component.html',
  styleUrls: ['./tarjeta-pelicula.component.scss'],
})
export class TarjetaPeliculaComponent {

  @Input()
  pelicula: Pelicula;

  @HostBinding('style.backgroundColor')
  private bgColor: string;

  @Input() set estaSeleccionada(value: boolean) {
    this.bgColor = value ? 'yellow' : 'white';
  };


  // https://angular.io/api/core/HostBinding
  @Input()
  @HostBinding('style.color')
  color: string;

  @Output() clickPelicula = new EventEmitter<number>();

  // https://angular.io/api/core/HostListener
  @HostListener('click', ['$event']) click(event): void {
    // console.log('click', event);
    this.clickPelicula.emit(this.pelicula.id);
  }

  // https://www.w3schools.com/tags/ref_eventattributes.asp
  @HostListener('dblclick', ['$event']) mouseover(event): void {
    // console.log('dblclick', event);
  }


}

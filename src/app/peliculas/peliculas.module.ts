import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ArrayLimitPipe } from './array-limit.pipe';
import { CtaPipe } from './cta.pipe';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { TarjetaPeliculaComponent } from './tarjeta-pelicula/tarjeta-pelicula.component';


@NgModule({
  declarations: [
    PeliculasComponent,
    TarjetaPeliculaComponent,
    CtaPipe,
    ArrayLimitPipe,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: PeliculasComponent},
    ]),
  ],
  exports: [PeliculasComponent, CtaPipe],
})
export class PeliculasModule {
}

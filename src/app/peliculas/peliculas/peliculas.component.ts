import { Component } from '@angular/core';
import { Pelicula } from '../models';


@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.scss'],
})
export class PeliculasComponent {

  idPeliculaSeleccionada: number;

  peliculas: Pelicula[] = [
    {id: 1, titulo: 'Jurassic Park', precio: 0.1},
    {id: 2, titulo: 'Titanic', precio: 0.123445},
    {id: 3, titulo: 'Cazafantasmas', descripcion: 'Comedia entretenida de los 80', precio: 13},
    {id: 4, titulo: 'Ghost', genero: 'miedo', precio: 234}];

  selectPelicula(id: number): void {
    this.idPeliculaSeleccionada = id;
  }


}

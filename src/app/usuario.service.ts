import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {

  usuarioId: string|null = null;
  roles: string[] = ['admin'];

}

import { Pipe, PipeTransform } from '@angular/core';
import { Empleado } from './models';

@Pipe({
  name: 'formatName',
})
export class FormatNamePipe implements PipeTransform {

  transform(empleado: Empleado, muestraApellidos = true): string {
    return `${empleado.nombre} ${muestraApellidos ? empleado.apellidos : ''}`;
  }

}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { DescargaEmpleadosGuard } from '../descarga-empleados.guard';
import { BorderChangeDirective } from './border-change.directive';
import { EmpleadoDetailComponent } from './empleado-detail/empleado-detail.component';
import { EmpleadoTarjetaComponent } from './empleado-tarjeta/empleado-tarjeta.component';
import { EmpleadosListComponent } from './empleados-list/empleados-list.component';
import { FormatNamePipe } from './format-name.pipe';


@NgModule({
  declarations: [
    EmpleadosListComponent,
    EmpleadoTarjetaComponent,
    FormatNamePipe,
    BorderChangeDirective,
    EmpleadoDetailComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: EmpleadosListComponent,
        data: {rolRequired: 'commercial'},
        resolve: {hola: DescargaEmpleadosGuard},
      },
      {
        path: ':nombre',
        component: EmpleadoDetailComponent,
        data: {titulo: 'Empleado', tieneFooter: true, rolRequired: 'admin'},
        canActivate: [AuthGuard],
      },
    ]),
  ],
})
export class EmpleadosModule {
}

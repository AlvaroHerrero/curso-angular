import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-empleado-detail',
  templateUrl: './empleado-detail.component.html',
  styleUrls: ['./empleado-detail.component.scss'],
})
export class EmpleadoDetailComponent {

  constructor(public activatedRoute: ActivatedRoute) {
  }

}

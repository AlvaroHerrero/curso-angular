import { Directive, ElementRef, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[appBorderChange]',
})
export class BorderChangeDirective {

  @HostBinding('style.backgroundColor')
  @Input() appBorderChange = 'green';

  @Output() elementClicked = new EventEmitter<number>();


  @Input() set nombre(value: string) {
    console.log(value);
  }

  @HostListener('click', ['$event']) click(): void {
    this.elementClicked.next(23);
  }

  constructor(private elementRef: ElementRef) {
  }

}

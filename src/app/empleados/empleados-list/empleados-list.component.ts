import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Empleado } from '../models';

@Component({
  selector: 'app-empleados-list',
  templateUrl: './empleados-list.component.html',
  styleUrls: ['./empleados-list.component.scss'],
})
export class EmpleadosListComponent {

  empleados: Empleado[] = [{nombre: 'Alfredo', apellidos: 'Gomez'}, {nombre: 'Pedro', apellidos: 'Gonzalez'}];

  directiveClicked(value: number): void {
    console.log(value);
  }

  constructor(private activatedRoute: ActivatedRoute) {
    console.log(this.activatedRoute.snapshot.data);
  }

}

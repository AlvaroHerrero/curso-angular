import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Empleado } from '../models';

@Component({
  selector: 'app-empleado-tarjeta',
  templateUrl: './empleado-tarjeta.component.html',
  styleUrls: ['./empleado-tarjeta.component.scss'],
})
export class EmpleadoTarjetaComponent implements OnInit {

  @Input() empleado: Empleado;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  openDetail(): void {
    this.router.navigate(
      ['/empleados', this.empleado.nombre],
      {
        queryParams: {esEmpleado: true, esJefe: false},
        fragment: 'footer',
        state: {datosPorState: 2345},
      });
  }

}

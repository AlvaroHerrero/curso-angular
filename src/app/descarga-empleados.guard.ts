import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DescargaEmpleadosGuard implements Resolve<any> {

  // Podemos devolver cualquier objecto, y si es un observable o promise, se entra en la ruta cuando emita
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    return of([12, 24, 345]).pipe(delay(5000));
  }

}

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Route, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';


const routes: Route[] = [
  {
    path: 'empleados',
    // lazy load
    loadChildren: () => import('./empleados/empleados.module').then(m => m.EmpleadosModule),
    data: {titulo: 'Empleados', tieneFooter: true},
  },
  {
    path: 'material',
    loadChildren: () => import('./material-elements/material-elements.module').then(m => m.MaterialElementsModule),
    data: {titulo: 'Material', tieneFooter: false},
  },
  {path: 'peliculas', loadChildren: () => import('./peliculas/peliculas.module').then(m => m.PeliculasModule), data: {titulo: 'Peliculas'}},
  {path: '**', redirectTo: ''},
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}

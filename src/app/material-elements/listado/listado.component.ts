import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { timer } from 'rxjs';

export interface DialogData {
  titulo: string;
}

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss'],
})
export class ListadoComponent implements OnInit {

  nombreListado: string;

  constructor(@Inject(MAT_DIALOG_DATA) data: DialogData,
              private dialogRef: MatDialogRef<ListadoComponent>) {
    this.nombreListado = data.titulo;
  }

  ngOnInit(): void {
    timer(5000).subscribe(() => {
      this.dialogRef.close(344);
    });
  }

}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { RouterModule } from '@angular/router';
import { ListadoComponent } from './listado/listado.component';
import { MaterialElementsComponent } from './material-elements/material-elements.component';

@NgModule({
  declarations: [
    MaterialElementsComponent,
    ListadoComponent,
  ],
  imports: [
    MatButtonModule,
    CommonModule,
    MatDividerModule,
    MatChipsModule,
    MatCardModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatExpansionModule,
    MatMenuModule,
    MatProgressBarModule,
    MatRippleModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatDialogModule,
    RouterModule.forChild([
      {path: '', component: MaterialElementsComponent},
    ]),
  ],
  exports: [MaterialElementsComponent, ListadoComponent],
})
export class MaterialElementsModule {
}
